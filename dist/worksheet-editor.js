var worksheetModule = angular.module('worksheet-editor', ['ui.sortable', 'ngResource', 'ui.bootstrap']);

//Отображение анкеты с панелью для её редактирования
worksheetModule.directive('worksheetEditor', [function () {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            ngModel: '=',
            type: '='
        },
        templateUrl: 'src/templates/worksheet-editor.html',
        controller: ['$scope', '$rootScope', function ($scope, $rootScope) {
            $scope.isCertification = $scope.type === 'certification';

            $scope.init = function () {
                $scope.worksheetData = angular.copy($scope.defaultWorksheetData);

                // открытие всех первых разделов в блоке
                angular.forEach($scope.worksheetData.blocks, function (block, blockIndex) {
                    angular.forEach(block.sections, function (section, sectionIndex) {
                        section.active = sectionIndex === 0;
                    });
                });

                $scope.fixOpenSection = [];
                $scope.idCloseSection = [];
                $scope.idActiveBlock = 0;
                $scope.indexDropBlock = -1;
                $scope.indexDropSection = -1;
                $scope.idActiveSection = 0;
                $scope.isEditBlock = false;
                $scope.isEditSection = false;
                $scope.isEditQuestion = false;
                $scope.disabledCancel = true;
                $scope.checkIncomingData();
                $scope.showInstruction();
            };

            //Правильная структура анкеты
            $scope.templatesWorksheetEditor = {
                'title': 'Новая анкета',
                'information':'',
                'blocks': [
                    {
                        'title': 'Название блока',
                        'id': 'Уникальный номер блока',
                        'sections': [
                            {
                                'title': 'Название раздела',
                                'id': 'Уникальный номер раздела',
                                'questions': [
                                    {
                                        'title': 'Название вопроса',
                                        'id': 'Уникальный номер вопроса',
                                        'type': 'Тип вопроса, может быть: radio, checkbox, input, textarea'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            };

            //Проверка входящих данных анкеты на соответсвие правильной структуре
            $scope.checkIncomingData = function () {
                if (!angular.isObject($scope.defaultWorksheetData)) {
                    console.error('Входящие данные должны быть объектом. Сравните структуру анкеты');
                    console.log($scope.templatesWorksheetEditor);
                } else if (!$scope.defaultWorksheetData.blocks) {
                    $scope.defaultWorksheetData.blocks = [];
                    $scope.worksheetData = angular.copy($scope.defaultWorksheetData);
                    $scope.addBlock(true);
                } else if (!angular.isArray($scope.defaultWorksheetData.blocks)) {
                    console.error('blocks должен быть массивом. Сравните структуру анкеты');
                    console.log($scope.templatesWorksheetEditor);
                } else if (!$scope.defaultWorksheetData.blocks.length) {
                    $scope.worksheetData = angular.copy($scope.defaultWorksheetData);
                    $scope.addBlock(true);
                } else {
                    for (var i = 0; i < $scope.defaultWorksheetData.blocks.length; i++) {
                        if (!angular.isObject($scope.defaultWorksheetData.blocks[i])) {
                            console.error(i + ' блок должен быть объектом. Сравните структуру анкеты');
                            console.log($scope.templatesWorksheetEditor);
                        } else if (!$scope.defaultWorksheetData.blocks[i].title && !$scope.defaultWorksheetData.blocks[i].sections) {
                            console.error('Отсутствует title и sections в ' + i + ' массиве. Сравните структуру анкеты');
                            console.log($scope.templatesWorksheetEditor);
                        } else if (!$scope.defaultWorksheetData.blocks[i].title) {
                            console.error('Отсутствует title в ' + i + ' массиве. Сравните структуру анкеты');
                            console.log($scope.templatesWorksheetEditor);
                        } else if (!$scope.defaultWorksheetData.blocks[i].sections) {
                            console.error('Отсутствует sections в ' + i + ' массиве. Сравните структуру анкеты');
                            console.log($scope.templatesWorksheetEditor);
                        } else if (!$scope.defaultWorksheetData.blocks[i].id) {
                            console.error('Отсутствует id в ' + i + ' массиве. Сравните структуру анкеты');
                            console.log($scope.templatesWorksheetEditor);
                        } else if (!angular.isArray($scope.defaultWorksheetData.blocks[i].sections)) {
                            console.error('В ' + i + ' блоке sections должен быть массивом. Сравните структуру анкеты');
                            console.log($scope.templatesWorksheetEditor);
                        } else {
                            for (var k = 0; k < $scope.defaultWorksheetData.blocks[i].sections.length; k++) {
                                if (!angular.isObject($scope.defaultWorksheetData.blocks[i].sections[k])) {
                                    console.error(i + ' блок должен хранить ' + k + ' раздел в виде объекта. Сравните структуру анкеты');
                                    console.log($scope.templatesWorksheetEditor);
                                } else if (!$scope.defaultWorksheetData.blocks[i].sections[k].title && !scope.defaultWorksheetData.blocks[i].sections[k].questions) {
                                    console.error('Отсутствует title и questions в ' + i + ' разделе ' + k + '-го массивa. Сравните структуру анкеты');
                                    console.log($scope.templatesWorksheetEditor);
                                } else if (!$scope.defaultWorksheetData.blocks[i].sections[k].title) {
                                    console.error('Отсутствует title в ' + i + ' разделе ' + k + '-го массивa. Сравните структуру анкеты');
                                    console.log($scope.templatesWorksheetEditor);
                                } else if (!$scope.defaultWorksheetData.blocks[i].sections[k].questions) {
                                    console.error('Отсутствует questions в ' + i + ' разделе ' + k + '-го массивa. Сравните структуру анкеты');
                                    console.log($scope.templatesWorksheetEditor);
                                } else if (!angular.isObject($scope.defaultWorksheetData.blocks[i].sections[k].questions)) {
                                    console.error('В ' + i + ' блока ' + k + '-го раздела questions должен быть массивом. Сравните структуру анкеты');
                                    console.log($scope.templatesWorksheetEditor);
                                } else {
                                    for (var j = 0; j < $scope.defaultWorksheetData.blocks[i].sections[k].questions.length; j++) {
                                        if (!angular.isObject($scope.defaultWorksheetData.blocks[i].sections[k].questions[j])) {
                                            console.error('В ' + i + ' блоке ' + k + '-го раздела ' + j + ' questions должен быть в виде объекта. Сравните структуру анкеты');
                                            console.log($scope.templatesWorksheetEditor);
                                        } else if (!$scope.defaultWorksheetData.blocks[i].sections[k].questions[j].title && !$scope.defaultWorksheetData.blocks[i].sections[k].questions[j].type) {
                                            console.error('Отсутствует title и type в ' + i + ' разделе ' + k + ' вопроса ' + j + '-го массивa. Сравните структуру анкеты');
                                            console.log($scope.templatesWorksheetEditor);
                                        } else if (!$scope.defaultWorksheetData.blocks[i].sections[k].questions[j].type) {
                                            console.error('Отсутствует type в ' + i + ' разделе ' + k + '-го вопроса ' + j + '-го массивa. Сравните структуру анкеты');
                                            console.log($scope.templatesWorksheetEditor);
                                        } else if (!$scope.defaultWorksheetData.blocks[i].sections[k].questions[j].title) {
                                            console.error('Отсутствует title в ' + i + ' разделе ' + k + '-го вопроса ' + j + '-го массивa. Сравните структуру анкеты');
                                            console.log($scope.templatesWorksheetEditor);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };


            // Проверка валидности
            $scope.valid = function () {
                if ($scope.userBlock) {
                    return !(($scope.userBlock.$invalid && $scope.isEditBlock) || ($scope.userSection.$invalid && $scope.isEditSection) || ($scope.userQuestion.$invalid && $scope.isEditQuestion));
                } else {
                    return true;
                }
            };

            // Показать интсрукцию
            $scope.showInstruction = function () {
                if ($scope.valid()) {
                    $scope.isActiveInstruction = true;
                    $scope.hideEditorViaButton();
                    angular.forEach($scope.worksheetData.blocks, function (block) {
                        block.active = false;
                    });
                }
            };

            // Сохранение данных
            $scope.save = function (id, element) {
                if (((element === 'block' && id === $scope.idActiveBlock && $scope.isEditBlock) || (element === 'section' && id === $scope.idActiveSection && $scope.isEditSection) || (element === 'question' && id === $scope.idActiveQuestion && $scope.isEditQuestion))) {
                    return false;
                } else if (!$scope.valid()) {
                    return false;
                } else {
                    var originalPath;
                    $scope.dataForButtonCancel = {};
                    $scope.defaultWorksheetData.blocks = angular.copy($scope.worksheetData.blocks);

                    // для кнопки отмена
                    if (element === 'block')  {
                        originalPath = $scope.defaultWorksheetData.blocks[id];
                        $scope.dataForButtonCancel.title = originalPath.title;
                        $scope.dataForButtonCancel.comments = originalPath.comments;
                        $scope.dataForButtonCancel.id = originalPath.id;
                    } else if (element === 'section') {
                        originalPath = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[id];
                        $scope.dataForButtonCancel.title = originalPath.title;
                        $scope.dataForButtonCancel.comments = originalPath.comments;
                        $scope.dataForButtonCancel.id = originalPath.id;
                    } else if ('question') {
                        $scope.dataForButtonCancel = angular.copy($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[id]);
                        delete($scope.dataForButtonCancel.active);
                    }

                    return true;
                }
            };

            var isInitInstruction = true;
            // Уведомляем об изменении содержимого анкеты
            $scope.$watch('defaultWorksheetData.instruction', function () {
                if (!isInitInstruction) {
                    $rootScope.$broadcast('changedWorksheet');
                }
                isInitInstruction = false;
            });

            // Мгновенная передача валидных данных в главный объект
            $scope.transferDataToMainObject = function (data) {
                if (!data || !$scope.valid()) {
                    return false;
                }

                if (data === 'question') {
                    $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion] = angular.copy($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion]);
                    delete($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].active);
                } else if (data === 'section') {

                    var editSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection];
                    var originalSection = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection];
                    originalSection.title = editSection.title;
                    originalSection.comments = editSection.comments;
                    originalSection.id = editSection.id;
                } else if (data === 'block') {
                    var editBlock = $scope.worksheetData.blocks[$scope.idActiveBlock];
                    var originalBlock = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock];
                    originalBlock.title = editBlock.title;
                    originalBlock.comments = editBlock.comments;
                    originalBlock.id = editBlock.id;
                }

                $rootScope.$broadcast('changedWorksheet');
            };

            // Отвечает за нажатие кнопки Отмена в панели
            $scope.clickButtonCancel = function (type) {
                var originalPath = $scope.worksheetData.blocks[$scope.idActiveBlock];
                var duplicatePath = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock];
                if (type === 'editBlock') {
                    originalPath.title = duplicatePath.title = $scope.dataForButtonCancel.title;
                    originalPath.comments = duplicatePath.comments = $scope.dataForButtonCancel.comments;
                    originalPath.id = duplicatePath.id = $scope.dataForButtonCancel.id;
                } else if (type === 'editSection') {
                    originalPath.sections[$scope.idActiveSection].title = duplicatePath.sections[$scope.idActiveSection].title = $scope.dataForButtonCancel.title;
                    originalPath.sections[$scope.idActiveSection].comments = duplicatePath.sections[$scope.idActiveSection].comments = $scope.dataForButtonCancel.comments;
                    originalPath.sections[$scope.idActiveSection].id = duplicatePath.sections[$scope.idActiveSection].id = $scope.dataForButtonCancel.id;
                } else if (type === 'editQuestion') {
                    originalPath.sections[$scope.idActiveSection].questions[$scope.idActiveQuestion] = angular.copy($scope.dataForButtonCancel);
                    duplicatePath.sections[$scope.idActiveSection].questions[$scope.idActiveQuestion] = angular.copy($scope.dataForButtonCancel);
                }
                $scope.hideEditor(type);
            };

            //Отвечает за отображение панели и скрытие, отмена изменений сделанных в предыдущем отображении
            $scope.assetOpenEditor = function () {
                if ($scope.isEditQuestion) {
                    $scope.isEditQuestion = false;
                    $scope.isEditSection = true;
                } else if (!$scope.isEditBlock && !$scope.isEditSection) {
                    $scope.isEditSection = true;
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections = angular.copy($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections);
                } else if ($scope.isEditBlock && !$scope.isEditSection) {
                    for (var i = 0; i < $scope.worksheetData.blocks.length; i++) {
                        $scope.worksheetData.blocks[i].active = false;
                    }
                    $scope.isEditBlock = false;
                    $scope.worksheetData.blocks[$scope.idActiveBlock].active = true;
                    $scope.isEditSection = true;
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections = angular.copy($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections);
                }
            };

            // Отвечает за скрытие панели редактора
            $scope.hideEditor = function (type) {
                var i;
                if (type === 'editBlock') {
                    $scope.isEditBlock = !$scope.isEditBlock;
                    for (i = $scope.worksheetData.blocks.length - 1; i >= 0; i--) {
                        $scope.worksheetData.blocks[i].title = $scope.defaultWorksheetData.blocks[i].title;
                        $scope.worksheetData.blocks[i].id = $scope.defaultWorksheetData.blocks[i].id;
                        $scope.worksheetData.blocks[i].comments = $scope.defaultWorksheetData.blocks[i].comments;
                    }
                } else if (type === 'deleteBlock') {
                    $scope.isEditBlock = !$scope.isEditBlock;
                    $scope.selectBlock(0);
                } else if (type === 'editSection') {
                    $scope.fixOpenSection[$scope.idActiveBlock] = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].active;
                    $scope.isEditSection = !$scope.isEditSection;
                    for (i = $scope.worksheetData.blocks[$scope.idActiveBlock].sections.length - 1; i >= 0; i--) {
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[i].title = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[i].title;
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[i].id = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[i].id;
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[i].comments = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[i].comments;
                    }
                } else if (type === 'addSection') {
                    $scope.isEditSection = !$scope.isEditSection;
                    $scope.fixOpenSection[$scope.idActiveBlock] = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].active;
                } else if (type === 'deleteSection') {
                    $scope.isEditSection = !$scope.isEditSection;
                } else if (type === 'editQuestion') {
                    var countQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.length;
                    var body = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions;
                    var defaultBody = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions;
                    $scope.isEditQuestion = !$scope.isEditQuestion;
                    for (i = countQuestion - 1; i >= 0; i--) {
                        body[i].title = defaultBody[i].title;
                        body[i].type = defaultBody[i].type;
                        body[i].variants = defaultBody[i].variants;
                        body[i].active = false;
                    }
                    $scope.fixOpenSection[$scope.idActiveBlock] = true;
                } else if (type === 'deleteQuestion') {
                    $scope.isEditQuestion = !$scope.isEditQuestion;
                }

                $rootScope.$broadcast('changedWorksheet');
            };

            //Отображает панель блока при клике на него
            $scope.showEditorBlock = function (type) {
                if ($scope.save(type, 'block')) {
                    $scope.disabledCancel = true;
                    $scope.isActiveInstruction = false;
                    $scope.assetOpenEditor();
                    $scope.isEditBlock = !$scope.isEditBlock;
                    $scope.nameEditBlock = $scope.worksheetData.blocks[type].title;
                    $scope.idEditBlock = $scope.worksheetData.blocks[type].id;
                    $scope.existEditCommentBlock = $scope.worksheetData.blocks[type].comments;
                    $scope.selectBlock(type);
                }
            };

            /*
             Отвечает за обновление порядкового номера блока при drag-and-drop
             совершает перестановку данных в исходном массиве
             ограничивает область за которую возможно совершить drag-and-drop
             */
            $scope.updateItemBlock = {
                stop: function (e, ui) {
                    var lengthBlocks = $scope.worksheetData.blocks.length;
                    $scope.defaultWorksheetData.blocks.splice(
                        ui.item.sortable.dropindex, 0,
                        $scope.defaultWorksheetData.blocks.splice(ui.item.sortable.index, 1)[0]);
                    for (var i = 0; i < lengthBlocks; i++) {
                        if ($scope.worksheetData.blocks[i].active) {
                            if ($scope.idActiveBlock !== i) {
                                $rootScope.$broadcast('changedWorksheet');
                            }
                            $scope.idActiveBlock = i;

                            break;
                        }
                    }
                },
                handle: '.block-grid'
            };

            /*
             Отвечает за обновление порядкового номера раздела при drag-and-drop
             совершает перестановку данных в исходном массиве
             ограничивает область за которую возможно совершить drag-and-drop
             */
            $scope.updateItemSection = {
                zIndex: -1,
                stop: function (e, ui) {
                    var lengthSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections.length;
                    $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections.splice(
                        ui.item.sortable.dropindex, 0,
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections.splice(ui.item.sortable.index, 1)[0]);
                    for (var i = 0; i < lengthSection; i++) {
                        if ($scope.worksheetData.blocks[$scope.idActiveBlock].sections[i].active) {
                            if ($scope.idActiveSection !== i) {
                                $rootScope.$broadcast('changedWorksheet');
                            }
                            $scope.idActiveSection = i;
                            break;
                        }
                    }
                    //перемещение раздела
                    if($scope.indexDropBlock !== -1 && $scope.idActiveBlock !== $scope.indexDropBlock) {
                        if ($scope.valid()) {
                            if (confirm("Хотите переместить раздел \"" + $scope.worksheetData.blocks[$scope.idActiveBlock].sections[0].title + "\" в блок \"" + $scope.worksheetData.blocks[$scope.indexDropBlock].title + "\"?")) {
                                var idSection = $scope.defaultWorksheetData.blocks[$scope.indexDropBlock].sections.length;

                                // сохраняем все измения
                                $scope.defaultWorksheetData.blocks[$scope.idActiveBlock] = angular.copy($scope.worksheetData.blocks[$scope.idActiveBlock]);

                                $scope.defaultWorksheetData.blocks[$scope.indexDropBlock].sections.push($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[0]);
                                $scope.worksheetData.blocks[$scope.indexDropBlock].sections.push($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[0]);

                                //удаляем раздел из старого блока
                                $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections.splice(0, 1);
                                $scope.worksheetData.blocks[$scope.idActiveBlock].sections.splice(0, 1);

                                //делаем активным блок куда переместили и раздел который переместили
                                $scope.showEditorBlock($scope.indexDropBlock);
                                $scope.disabledCancel = true;
                                $scope.assetOpenEditor();
                                $scope.nameEditSection = $scope.worksheetData.blocks[$scope.indexDropBlock].sections[idSection].title;
                                $scope.idEditSection = $scope.worksheetData.blocks[$scope.indexDropBlock].sections[idSection].id;
                                $scope.existEditCommentSection = $scope.worksheetData.blocks[$scope.indexDropBlock].sections[idSection].comments;
                                $scope.toggleOpenSection(idSection);

                                $rootScope.$broadcast('changedWorksheet');
                            }
                        }
                        $scope.indexDropBlock = -1;
                    }
                },
                handle: '.section-grid'
            };

            //сохраняет индекс блока на который переместили раздел
            $scope.saveIndexDropBlock = function(type) {
                $scope.indexDropBlock = type;
            };

            /*
             Отвечает за обновление порядкового номера вопроса при drag-and-drop
             совершает перестановку данных в исходном массиве
             ограничивает область за которую возможно совершить drag-and-drop
             */
            $scope.updateItemQuestion = {
                zIndex: -1,
                stop: function (e, ui) {
                    var lengthQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.length;
                    $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.splice(
                        ui.item.sortable.dropindex, 0,
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.splice(ui.item.sortable.index, 1)[0]);
                    for (var i = 0; i < lengthQuestion; i++) {
                        if ($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[i].active) {
                            $scope.idActiveQuestion = i;
                            if ($scope.idActiveQuestion !== i) {
                                $rootScope.$broadcast('changedWorksheet');
                            }
                            $rootScope.$broadcast('changedWorksheet');
                        }
                    }
                    //перемещение вопроса
                    if($scope.indexDropSection !== -1 && $scope.idActiveSection !== $scope.indexDropSection) {
                        if ($scope.valid()) {
                            var oldIdQuestion = 0;
                            if ($scope.idActiveSection < $scope.indexDropSection) {
                                oldIdQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.length-1;
                            }
                            if (confirm("Хотите переместить вопрос \"" + $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[oldIdQuestion].title + "\" в раздел \"" + $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.indexDropSection].title + "\"?")) {
                                var newIdQuestion;

                                // сохраняем все измения сделанные до нового вопроса
                                $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection] = angular.copy($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection]);

                                if ($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.indexDropSection].questions) {
                                    newIdQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.indexDropSection].questions.length;
                                } else {
                                    newIdQuestion = 0;
                                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.indexDropSection].questions = [];
                                    $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.indexDropSection].questions = [];
                                }
                                //копирование вопроса в другой раздел
                                $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.indexDropSection].questions.push($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[oldIdQuestion]);
                                $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.indexDropSection].questions.push($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[oldIdQuestion]);

                                //удаление вопроса из старого раздела
                                $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.splice(oldIdQuestion, 1);
                                $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.splice(oldIdQuestion, 1);

                                //вопрос и раздел стают активными и вопрос открывается в редакторе
                                $scope.disabledCancel = true;
                                $scope.toggleOpenSection($scope.indexDropSection);
                                $scope.isEditBlock = false;
                                $scope.isEditSection = false;
                                $scope.isEditQuestion = true;
                                $scope.nameEditQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.indexDropSection].questions[newIdQuestion].title;
                                $scope.editTypeQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.indexDropSection].questions[newIdQuestion].type;
                                $scope.selectQuestion(newIdQuestion);
                                $rootScope.$broadcast('changedWorksheet');
                            }
                        }
                        $scope.indexDropSection = -1;
                    }
                },
                handle: '.question-grid'
            };

            //сохраняет индекс секции на который переместили вопрос
            $scope.saveIndexDropSection = function(type) {
                $scope.indexDropSection = type;
            };

            //Функция сохраняет данные о текущем вопросе открытом в панели
            $scope.selectQuestion = function (index) {
                var lengthQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.length;
                for (var i = 0; i < lengthQuestion; i++) {
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[i].active = false;
                }
                $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[index].active = true;
                $scope.idActiveQuestion = index;
            };

            //Функция сохраняет данные о текущем блоке открытом в панели
            $scope.selectBlock = function (index) {
                var lengthBlocks = $scope.worksheetData.blocks.length;
                $scope.worksheetData.blocks = angular.copy($scope.defaultWorksheetData.blocks);
                for (var i = 0; i < lengthBlocks; i++) {
                    $scope.worksheetData.blocks[i].active = false;
                    $scope.fixOpenSection[i] = false;
                }
                if ($scope.worksheetData.blocks[0]) {
                    $scope.worksheetData.blocks[index].active = true;
                }
                $scope.isEditSection = false;
                $scope.idActiveBlock = index;
            };

            //Открывает/закрывает раздел и отменяет предыдущие изменения
            $scope.toggleOpenSection = function (index) {
                var close;
                var lengthSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections.length;
                if ($scope.fixOpenSection[$scope.idActiveBlock] && index === $scope.idActiveSection) {
                    close = true;
                } else {
                    close = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[index].active;
                }
                for (var i = 0; i < lengthSection; i++) {
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[i].active = false;
                    $scope.idCloseSection[i] = false;
                }
                if (close) {
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[index].active = false;
                    $scope.idCloseSection[index] = true;
                } else {
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[index].active = true;
                }
                if ($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection]) {
                    if ($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions)
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions = angular.copy($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions);
                }
                for (i = $scope.fixOpenSection.length - 1; i >= 0; i--) {
                    $scope.fixOpenSection[i] = false;
                }
                $scope.idActiveSection = index;
            };

            //Отвечает за мгновенное отображение измененных данных блока в анкете
            $scope.changeBlock = function () {
                $scope.disabledCancel = false;
                var editBlock = $scope.worksheetData.blocks[$scope.idActiveBlock];
                editBlock.title = $scope.nameEditBlock;
                editBlock.comments = $scope.existEditCommentBlock;
                editBlock.id = $scope.idEditBlock;
                $scope.transferDataToMainObject('block');
            };

            //Добавляет блок
            $scope.addBlock = function (disableActive) {
                var idBlock;
                if ($scope.valid()) {
                    // сохраняем все измения сделанные до нового блока
                    $scope.defaultWorksheetData.blocks = angular.copy($scope.worksheetData.blocks);

                    if ($scope.defaultWorksheetData.blocks) {
                        idBlock = $scope.defaultWorksheetData.blocks.length;
                    } else {
                        idBlock = 0;
                        $scope.defaultWorksheetData.blocks = [];
                        $scope.worksheetData.blocks = [];
                    }
                    $scope.defaultWorksheetData.blocks.push({
                        'id': 'b' + idBlock,
                        'title': 'Новый блок',
                        'comments': false,
                        'sections': [
                            {
                                'id': 'b' + idBlock + 's0',
                                'title': 'Новый раздел',
                                'comments': false,
                                'active': true,
                                'questions': [
                                    {
                                        'variants': $scope.defaultVariants().yesNo.answer,
                                        'id': 'b' + idBlock + 's0q0',
                                        'title': 'Вопрос',
                                        'type': $scope.addTypeQuestion
                                    }
                                ]
                            }
                        ]
                    });
                    $scope.worksheetData.blocks.push({
                        'id': 'b' + idBlock,
                        'title': 'Новый блок',
                        'comments': false,
                        'sections': [
                            {
                                'id': 'b' + idBlock + 's0',
                                'title': 'Новый раздел',
                                'comments': false,
                                'active': true,
                                'questions': [
                                    {
                                        'variants': $scope.defaultVariants().yesNo.answer,
                                        'id': 'b' + idBlock + 's0q0',
                                        'title': 'Вопрос',
                                        'type': $scope.addTypeQuestion
                                    }
                                ]
                            }
                        ]
                    });
                    $scope.idActiveSection = 0;
                    $scope.disabledCancel = true;

                    if (!disableActive) {
                        $scope.isActiveInstruction = false;
                        $scope.assetOpenEditor();
                        $scope.isEditBlock = !$scope.isEditBlock;
                        $scope.nameEditBlock = $scope.worksheetData.blocks[idBlock].title;
                        $scope.idEditBlock = $scope.worksheetData.blocks[idBlock].id;
                        $scope.existEditCommentBlock = $scope.worksheetData.blocks[idBlock].comments;
                    }
                    $scope.selectBlock(idBlock);
                    $rootScope.$broadcast('changedWorksheet');
                }
            };

            //Сохранение изменений в блоке
            $scope.editBlock = function () {
                var editBlock = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock];
                editBlock.id = $scope.idEditBlock;
                editBlock.title = $scope.nameEditBlock;
                editBlock.comments = $scope.existEditCommentBlock;
                $scope.hideEditor('editBlock');
            };

            //Удаление блока
            $scope.deleteBlock = function () {
                $scope.defaultWorksheetData.blocks.splice($scope.idActiveBlock, 1);
                $scope.worksheetData.blocks.splice($scope.idActiveBlock, 1);
                $scope.hideEditor('deleteBlock');
            };

            //Функция отображает панель при клике на раздел
            $scope.showEditorSection = function (type) {
                if ($scope.save(type, 'section')) {
                    $scope.disabledCancel = true;
                    $scope.assetOpenEditor();
                    for (var i = $scope.worksheetData.blocks[$scope.idActiveBlock].sections.length - 1; i >= 0; i--) {
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[i].title = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[i].title;
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[i].id = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[i].id;
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[i].comments = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[i].comments;
                    }
                    $scope.nameEditSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[type].title;
                    $scope.idEditSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[type].id;
                    $scope.existEditCommentSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[type].comments;
                    $scope.toggleOpenSection(type);
                } else if (type === $scope.idActiveSection && $scope.isEditSection && $scope.userSection.$valid) {
                    $scope.toggleOpenSection(type);
                }
            };

            //Отвечает за мгновенное отображение измененных данных раздела в анкете
            $scope.changeSection = function () {
                $scope.disabledCancel = false;
                var editSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection];
                editSection.title = $scope.nameEditSection;
                editSection.comments = $scope.existEditCommentSection;
                editSection.id = $scope.idEditSection;

                $scope.transferDataToMainObject('section');
            };

            //Добавляет раздел
            $scope.addSection = function () {
                if ($scope.valid()) {
                    var idSection = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections.length;

                    // сохраняем все измения сделанные до нового раздела
                    $scope.defaultWorksheetData.blocks[$scope.idActiveBlock] = angular.copy($scope.worksheetData.blocks[$scope.idActiveBlock]);

                    $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections.push({
                        'id': 'b' + $scope.idActiveBlock + 's' + idSection,
                        'title': 'Новый раздел',
                        'comments': false,
                        'questions': [
                            {
                                'variants': $scope.defaultVariants().yesNo.answer,
                                'id': 'b' + $scope.idActiveBlock + 's' + idSection + 'q0',
                                'title': 'Вопрос',
                                'type': $scope.addTypeQuestion
                            }
                        ]
                    });
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections.push({
                        'id': 'b' + $scope.idActiveBlock + 's' + idSection,
                        'title': 'Новый раздел',
                        'comments': false,
                        'questions': [
                            {
                                'variants': $scope.defaultVariants().yesNo.answer,
                                'id': 'b' + $scope.idActiveBlock + 's' + idSection + 'q0',
                                'title': 'Вопрос',
                                'type': $scope.addTypeQuestion
                            }
                        ]
                    });
                    $scope.disabledCancel = true;
                    $scope.assetOpenEditor();
                    $scope.nameEditSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[idSection].title;
                    $scope.idEditSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[idSection].id;
                    $scope.existEditCommentSection = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[idSection].comments;
                    $scope.toggleOpenSection(idSection);
                    $rootScope.$broadcast('changedWorksheet');
                }
            };

            //Сохраняет изменения раздела
            $scope.editSection = function () {
                var editSection = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection];
                editSection.id = $scope.idEditSection;
                editSection.title = $scope.nameEditSection;
                editSection.comments = $scope.existEditCommentSection;
                $scope.hideEditor('addSection');
            };

            //Удаление раздела
            $scope.deleteSection = function () {
                $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections.splice($scope.idActiveSection, 1);
                $scope.worksheetData.blocks[$scope.idActiveBlock].sections.splice($scope.idActiveSection, 1);
                $scope.hideEditor('deleteSection');
            };

            $scope.templateAnswer = 'false';
            $scope.addTypeQuestion = 'radio';

            //Шаблон ответов в вопросе
            $scope.defaultVariants = function () {
                return {
                    'yesNo': {
                        'answer': [
                            {
                                'id': 'b' + $scope.idActiveBlock + 's' + $scope.idActiveSection + 'v0',
                                'title': 'Да',
                                'score': 1
                            },
                            {
                                'id': 'b' + $scope.idActiveBlock + 's' + $scope.idActiveSection + 'v1',
                                'title': 'Нет',
                                'score': 0
                            },
                            {
                                'id': 'b' + $scope.idActiveBlock + 's' + $scope.idActiveSection + 'v3',
                                'title': 'Не знаю',
                                'score': 0
                            }
                        ],
                        'id': 'yesNo',
                        'title': 'Да, Нет, Не знаю'
                    }
                };
            };

            //Добавление вопроса
            $scope.addQuestion = function () {
                if ($scope.valid()) {
                    var block = $scope.worksheetData.blocks[$scope.idActiveBlock];
                    var section = block.sections[$scope.idActiveSection];
                    var idQuestion;

                    // сохраняем все измения сделанные до нового вопроса
                    $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection] = angular.copy(section);

                    if (section.questions) {
                        idQuestion = section.questions.length;
                    } else {
                        idQuestion = 0;
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions = [];
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions = [];
                    }
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.push({
                        'variants': $scope.defaultVariants().yesNo.answer,
                        'id': 'b' + $scope.idActiveBlock + 's' + $scope.idActiveSection + 'q' + idQuestion,
                        'title': 'Вопрос',
                        'type': $scope.addTypeQuestion
                    });
                    $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.push({
                        'variants': $scope.defaultVariants().yesNo.answer,
                        'id': 'b' + $scope.idActiveBlock + 's' + $scope.idActiveSection + 'q' + idQuestion,
                        'title': 'Вопрос',
                        'type': $scope.addTypeQuestion
                    });
                    $scope.disabledCancel = true;
                    $scope.isEditBlock = false;
                    $scope.isEditSection = false;
                    $scope.isEditQuestion = true;
                    $scope.templateAnswer = 'yesNo';
                    $scope.nameEditQuestion = section.questions[idQuestion].title;
                    $scope.editTypeQuestion = section.questions[idQuestion].type;
                    $scope.selectQuestion(idQuestion);
                    $rootScope.$broadcast('changedWorksheet');
                }
            };

            //Отвечает за мгновенное отображение данных вопроса в анкете при изменении их с редактора
            $scope.changeQuestion = function () {
                $scope.disabledCancel = false;
                var filesOptions,
                    editQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion];
                    editQuestion.title = $scope.nameEditQuestion;
                    editQuestion.type = $scope.editTypeQuestion;

                if ($scope.editTypeQuestion === 'input' || $scope.editTypeQuestion === 'textarea') {
                    delete($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants);
                    delete($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].files);
                } else if ($scope.editTypeQuestion === 'files') {
                    delete($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants);
                    if (angular.isObject($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].files)) {
                        filesOptions = $scope.currentFiles = angular.copy($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].files);
                    } else {
                        filesOptions = $scope.currentFiles = {
                            max: 1,
                            required: true,
                            accept: 'image'
                        };
                    }
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].files = filesOptions;
                } else if ($scope.templateAnswer === 'yesNo') {
                    delete($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].files);
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants = $scope.defaultVariants().yesNo.answer;
                } else {
                    delete($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].files);
                    if (angular.isObject($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants)) {
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants = angular.copy($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants);
                    } else {
                        $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants = $scope.defaultVariants().yesNo.answer;
                    }
                }
                $scope.transferDataToMainObject('question');
            };

            // Отвечает за мгновенное отображение данных названия вопроса
            $scope.changeQuestionTitle = function () {
                $scope.disabledCancel = false;
                $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].title = $scope.nameEditQuestion;
                $scope.transferDataToMainObject('question');
            };

            //Функция отображает панель при клике на вопрос
            $scope.showEditorQuestion = function (index) {
                if ($scope.save(index, 'question')) {
                    $scope.disabledCancel = true;
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection] = angular.copy($scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection]);
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].active = true;
                    $scope.isEditBlock = false;
                    $scope.isEditSection = false;
                    $scope.isEditQuestion = true;
                    $scope.templateAnswer = false;
                    $scope.selectQuestion(index);
                    $scope.nameEditQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[index].title;
                    $scope.editTypeQuestion = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[index].type;
                    $scope.currentFiles = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[index].files;
                }

            };

            //Удаление вопроса
            $scope.deleteQuestion = function () {
                $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.splice($scope.idActiveQuestion, 1);
                $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions.splice($scope.idActiveQuestion, 1);
                $scope.hideEditor('deleteQuestion');
            };

            //Сохранение вопроса
            $scope.editQuestion = function () {
                var editQuestion = $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion];
                editQuestion.title = $scope.nameEditQuestion;
                editQuestion.type = $scope.editTypeQuestion;
                editQuestion.variants = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants;
                $scope.hideEditor('editQuestion');
            };

            //Удаление ответа на вопрос
            $scope.addDeleteAnswer = function (index) {
                $scope.disabledCancel = false;
                var editAnswer = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants;
                editAnswer.splice(index, 1);
                $rootScope.$broadcast('changedWorksheet');
            };

            //Добавление ответа на вопрос
            $scope.addAnswer = function () {
                $scope.disabledCancel = false;
                var editAnswer = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants;
                var lengthAnswer;
                if (editAnswer) {
                    lengthAnswer = editAnswer.length;
                } else {
                    lengthAnswer = 0;
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants = [];
                }
                $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants.push({
                    'id': 'b' + $scope.idActiveBlock + 's' + $scope.idActiveSection + 'q' + $scope.idActiveQuestion + 'v' + lengthAnswer,
                    'title': '',
                    'score': 0
                });
                $rootScope.$broadcast('changedWorksheet');
            };

            //Отображает шаблон ответов
            $scope.changeTemplateAnswer = function () {
                $scope.disabledCancel = false;
                if ($scope.changeTemplateAnswer != 'false') {
                    $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants = $scope.defaultVariants().yesNo.answer;
                }
                $scope.transferDataToMainObject('question');
            };

            $scope.changeAnswer = function () {
                $scope.disabledCancel = false;
                $scope.transferDataToMainObject('question');
            };

            //Скрывает панель редактирования при нажатии на кнопку на панели
            $scope.hideEditorViaButton = function () {
                if ($scope.valid()) {
                    if ($scope.isEditBlock) {
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].title = $scope.worksheetData.blocks[$scope.idActiveBlock].title;
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].id = $scope.worksheetData.blocks[$scope.idActiveBlock].id;
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].comments = $scope.worksheetData.blocks[$scope.idActiveBlock].comments;
                    } else if ($scope.isEditSection) {
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].title = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].title;
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].id = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].id;
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].comments = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].comments;
                    } else if ($scope.isEditQuestion) {
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].title = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].title;
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].type = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].type;
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants = angular.copy($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].variants);
                        $scope.defaultWorksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].files = angular.copy($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].files);
                    }
                    $scope.isEditBlock = false;
                    $scope.isEditSection = false;
                    $scope.isEditQuestion = false;
                    if ($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection]) {
                        $scope.fixOpenSection[$scope.idActiveBlock] = $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].active;
                        if ($scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion]) {
                            $scope.worksheetData.blocks[$scope.idActiveBlock].sections[$scope.idActiveSection].questions[$scope.idActiveQuestion].active = false;
                        }
                    }
                }
            };

            // Максимальное количество файлов массивом
            $scope.getNumberInput = function (number) {
                var array = [''];

                if (number) {
                    array = new Array(+number);
                }

                return array;
            };

            //Отображает панель при клике на кнопку на панели
            $scope.showEditorViaButton = function () {
                $scope.showEditorBlock($scope.idActiveBlock);
                for (var i = $scope.fixOpenSection.length - 1; i >= 0; i--) {
                    $scope.fixOpenSection[i] = false;
                }
            };

            //Инициализация анкеты
            if (!angular.isObject($scope.ngModel)) {
                console.error('Передайте в директиву объект');
            }
            else if ($scope.ngModel.$promise) {
                $scope.ngModel.$promise.then(function (data) {
                    $scope.defaultWorksheetData = data;
                    $scope.init();
                });
            } else {
                $scope.defaultWorksheetData = $scope.ngModel;
                $scope.init();
            }
        }]
    };
}]);

//Отвечает за изменеие размера панели редактирования
worksheetModule.directive('resizableEditor', [function () {
    return {
        replace: true,
        restrict: 'A',
        link: function ($scope, $element, $attrs) {
            jQuery(function () {
                jQuery('.resizable').resizable({
                    handles: 'w',
                    minWidth: 350,
                    resize: function (event, ui) {
                        $(this).css('width', '');
                        $('.text-answer').width($(this).width()*0.98-100);
                    }
                });
            });
        }
    };
}]);
angular.module('worksheet-editor').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('src/templates/worksheet-editor.html',
    "<div ng-model=\"ngModel\" class=\"worksheet-editor\"><div class=\"tab-block\" class=\"nav nav-pills\"><ul class=\"nav nav-pills\"><li ng-click=\"showInstruction()\" ng-class=\"{'active': isActiveInstruction }\"><a><i class=\"fa fa-info-circle\"></i> <i>Инструкция</i> <i class=\"fa fa-pencil\" ng-if=\"isActiveInstruction\"></i></a></li><ul class=\"nav nav-pills enable-ui-sortable\" ui-sortable=\"updateItemBlock\" ng-model=\"worksheetData.blocks\"><li ng-repeat=\"block in worksheetData.blocks track by $index\" ng-mouseup=\"saveIndexDropBlock($index)\" ng-click=\"showEditorBlock($index)\" ng-class=\"{'active': worksheetData.blocks[$index].active}\"><a ng-class=\"{'active-block': worksheetData.blocks[$index].active && isEditBlock && userBlock.$invalid}\"><span class=\"block-grid\"></span> {{$index+1}}. {{block.title}} <i class=\"fa fa-pencil\" ng-show=\"worksheetData.blocks[$index].active && isEditBlock\"></i></a></li></ul><li ng-click=\"addBlock()\"><a>+</a></li></ul></div><div class=\"tab-section\"><div ng-if=\"isActiveInstruction\"><text-angular ng-model=\"defaultWorksheetData.instruction\"></text-angular></div><div class=\"panel-group\" ui-sortable=\"updateItemSection\" ng-model=\"worksheetData.blocks[idActiveBlock].sections\"><div class=\"panel panel-default\" style=\"background-color: transparent\" ng-class=\"{'active-section-border': (section.active && isEditSection && userSection.$invalid) || (idCloseSection[$index] && isEditSection && userSection.$invalid)}\" ng-repeat=\"section in worksheetData.blocks[idActiveBlock].sections track by $index\" ng-show=\"!isActiveInstruction\"><div class=\"panel-heading\" ng-mouseup=\"saveIndexDropSection($index)\" ng-class=\"{'active-section': (section.active && isEditSection && userSection.$invalid) || (idCloseSection[$index] && isEditSection && userSection.$invalid)}\"><h4 class=\"panel-title\"><a class=\"accordion-toggle\"><span class=\"section-grid\"></span><span ng-click=\"showEditorSection($index)\">{{idActiveBlock+1}}.{{$index+1}}. {{section.title}} <i class=\"fa fa-pencil\" ng-show=\"section.active && isEditSection || idCloseSection[$index] && isEditSection\"></i></span></a></h4></div><div class=\"panel-collapse\" ng-show=\"section.active\"><div class=\"panel-body\"><div class=\"tab-question\"><div ui-sortable=\"updateItemQuestion\" ng-model=\"section.questions\"><div ng-repeat=\"question in section.questions track by $index\" ng-class=\"{'active-question-border': question.active && isEditQuestion && userQuestion.$invalid, 'active-question-background': question.active && isEditQuestion }\"><p ng-class=\"{'active-question': question.active && isEditQuestion && userQuestion.$invalid}\"><span ng-click=\"showEditorQuestion($index)\"><span class=\"question-grid\"></span>{{idActiveBlock+1}}.{{idActiveSection+1}}.{{$index+1}}. <span>{{question.title}} <i class=\"fa fa-pencil\" ng-show=\"question.active && isEditQuestion\"></i></span></span></p><div ng-repeat=\"variant in question.variants\" class=\"{{question.type}} tab-variant\"><label><input type=\"{{question.type}}\" name=\"{{question.id}}\" value=\"{{variant.score}}\"> {{variant.title}}</label><span>{{variant.score}}</span></div><div ng-if=\"question.type == 'input'\" class=\"tab-variant\"><input type=\"text\" class=\"form-control\"></div><div ng-if=\"question.type == 'textarea'\" class=\"tab-variant\"><textarea class=\"form-control\"></textarea></div><div ng-if=\"question.type == 'files'\" class=\"form-group tab-variant worksheet-files\"><p>Загрузите <span ng-if=\"question.files.accept === 'image'\">изображение</span> <span ng-if=\"question.files.accept === 'video'\">видео</span> <span ng-if=\"question.files.accept === 'audio'\">аудио</span> :</p><div ng-repeat=\"i in getNumberInput(question.files.max) track by $index\"><input type=\"file\" class=\"form-control\" accept=\"{{ question.files.accept }}/*\"></div></div><p class=\"help-block\" ng-if=\"question.note\"><span class=\"label label-info\">Примечание:</span> {{question.note}}</p></div><div class=\"send-comment\"><a ng-click=\"addQuestion()\" class=\"btn btn-xs btn-default\" role=\"button\"><span class=\"add-new-section-question\"><i class=\"fa fa-plus\"></i>Добавить новый элемент</span></a></div></div><form role=\"form\" class=\"send-comment\" ng-repeat-end ng-hide=\"!(section.comments)\"><h4>Комментарии по разделу анкеты</h4><div class=\"form-group\"><textarea class=\"form-control\"></textarea></div><p class=\"help-block\"><span class=\"label label-info\">Примечание:</span> Предоставьте, пожалуйста, как можно более подробные комментарии, касающиеся этого раздела анкеты</p></form></div></div></div></div></div><div class=\"send-comment\" ng-show=\"worksheetData.blocks.length && !isActiveInstruction\"><a ng-click=\"addSection()\" class=\"btn btn-xs btn-default\" role=\"button\"><span class=\"add-new-section-question\"><i class=\"fa fa-plus\"></i>Добавить новый элемент</span></a></div><form role=\"form\" class=\"send-comment\" ng-hide=\"!(worksheetData.blocks[idActiveBlock].comments) || isActiveInstruction\"><h4>Комментарии по блоку анкеты</h4><div class=\"form-group\"><textarea class=\"form-control\" placeholder></textarea></div><p class=\"help-block\"><span class=\"label label-info\">Примечание:</span> Предоставьте, пожалуйста, как можно более подробные комментарии, касающиеся этого блока анкеты</p></form></div><div resizable-editor id=\"panel\" class=\"resizable\" ng-show=\"isEditBlock || isEditSection || isEditQuestion\"><span class=\"button-close-editor\" ng-click=\"hideEditorViaButton()\" title=\"Закрыть\"></span><div class=\"edit-blocks\" ng-show=\"isEditBlock\"><form name=\"userBlock\" novalidate><div class=\"form-group\" ng-class=\"{'has-error': userBlock.name.$invalid}\"><input name=\"name\" ng-change=\"changeBlock()\" type=\"text\" class=\"form-control\" ng-model=\"nameEditBlock\" required><p ng-show=\"userBlock.name.$error.required\" class=\"help-block\">Имя слишком короткое</p></div><div class=\"form-group\" ng-show=\"debug\" ng-class=\"{'has-error': userBlock.id.$invalid}\" ng-if=\"!isCertification\"><label for=\"block-id\">ID</label><div class=\"col-sm-10\"><input name=\"id\" ng-change=\"changeBlock()\" popover-placement=\"left\" popover=\"Для разработчиков\" popover-trigger=\"focus\" type=\"text\" class=\"form-control input-sm\" id=\"block-id\" ng-model=\"idEditBlock\" required></div><p ng-show=\"userBlock.id.$error.required\" class=\"help-block\">Идентификатор слишком короткий</p></div><div class=\"checkbox\" ng-show=\"!isCertification\"><label><input type=\"checkbox\" ng-change=\"changeBlock()\" ng-model=\"existEditCommentBlock\"> Разрешить комментарии к блоку</label></div><div class=\"row\"><div class=\"col-xs-6 col-md-6\"><input class=\"btn btn-warning\" ng-click=\"clickButtonCancel('editBlock')\" type=\"button\" value=\"Отменить\" ng-disabled=\"disabledCancel\"></div><div class=\"col-xs-6 col-md-6\"><input class=\"btn btn-danger\" type=\"button\" value=\"Удалить\" ng-click=\"deleteBlock()\"></div></div></form></div><div class=\"edit-sections\" ng-show=\"isEditSection\"><form name=\"userSection\" novalidate><div class=\"form-group\" ng-class=\"{'has-error': userSection.name.$invalid}\"><textarea name=\"name\" ng-change=\"changeSection()\" class=\"form-control\" ng-model=\"nameEditSection\" rows=\"2\" required></textarea><p ng-show=\"userSection.name.$error.required\" class=\"help-block\">Имя слишком короткое</p></div><div class=\"form-group\" ng-show=\"debug\" ng-class=\"{'has-error': userSection.id.$invalid}\" ng-if=\"!isCertification\"><label for=\"section-id\">ID</label><div class=\"col-sm-10\"><input type=\"text\" name=\"id\" class=\"form-control input-sm\" ng-change=\"changeSection()\" popover-placement=\"left\" popover=\"Для разработчиков\" popover-trigger=\"focus\" id=\"section-id\" ng-model=\"idEditSection\" required></div><p ng-show=\"userSection.id.$error.required\" class=\"help-block\">Идентификатор слишком короткий</p></div><div class=\"checkbox\" ng-show=\"!isCertification\"><label><input type=\"checkbox\" ng-change=\"changeSection()\" ng-model=\"existEditCommentSection\"> Разрешить комментарии к разделу</label></div><div class=\"row\"><div class=\"col-xs-6 col-md-6\"><input class=\"btn btn-warning\" ng-click=\"clickButtonCancel('editSection')\" type=\"button\" value=\"Отменить\" ng-disabled=\"disabledCancel\"></div><div class=\"col-xs-6 col-md-6\"><input class=\"btn btn-danger\" type=\"button\" value=\"Удалить\" ng-click=\"deleteSection()\"></div></div></form></div><div class=\"edit-questions\" ng-show=\"isEditQuestion\"><form role=\"form\" name=\"userQuestion\" novalidate><div class=\"form-group\" ng-class=\"{'has-error': userQuestion.name.$invalid}\"><textarea class=\"form-control\" ng-change=\"changeQuestionTitle()\" name=\"name\" ng-model=\"nameEditQuestion\" rows=\"2\" required></textarea><p ng-show=\"userQuestion.name.$error.required\" class=\"help-block\">Имя слишком короткое</p></div><div><div class=\"form-group\"><label for=\"type-edit-question\">Тип</label><select class=\"form-control\" ng-change=\"changeQuestion()\" ng-model=\"editTypeQuestion\" id=\"type-edit-question\"><option value=\"radio\">Один из нескольких</option><option value=\"checkbox\">Несколько из нескольких</option><option value=\"input\" ng-if=\"!isCertification\">Строка</option><option value=\"textarea\" ng-if=\"!isCertification\">Несколько строк</option><option value=\"files\" ng-if=\"!isCertification\">Файлы</option></select></div><div ng-if=\"editTypeQuestion === ('files')\"><ng-form name=\"userFiles\"><div class=\"form-group\"><label for=\"accept-edit-question\">Вид</label><select class=\"form-control\" ng-model=\"currentFiles.accept\" ng-change=\"changeAnswer(); currentFiles.max = 1\" id=\"accept-edit-question\"><option value=\"image\">Изображение</option><option value=\"video\">Видео</option><option value=\"audio\">Аудио</option></select></div><div class=\"form-group\" ng-class=\"{'has-error': userFiles.max.$invalid}\"><label for=\"max-edit-question\">Максимальное кол. элементов</label><input type=\"number\" class=\"form-control\" name=\"max\" ng-model=\"currentFiles.max\" ng-change=\"changeAnswer()\" min=\"1\" max=\"10\" id=\"max-edit-question\" ng-disabled=\"currentFiles.accept !== 'image'\" required><p ng-show=\"userFiles.max.$error.required\" class=\"help-block number\">Должно быть число</p></div><div class=\"form-group\"><div class=\"checkbox\"><label><input type=\"checkbox\" id=\"requiredFiles\" name=\"requiredFiles\" ng-model=\"currentFiles.required\" ng-true-value=\"true\"> Поле обязательное к заполнению</label></div></div></ng-form></div><!-- Шаблоны\n" +
    "                <div class=\"form-group\"\n" +
    "                     ng-hide=\"editTypeQuestion === ('textarea') || editTypeQuestion === ('input') || editTypeQuestion === ('files')\">\n" +
    "                    <label for=\"template-add-question\">Шаблон</label>\n" +
    "                    <select ng-model=\"templateAnswer\" ng-change=\"changeTemplateAnswer()\" id=\"template-add-question\"\n" +
    "                            class=\"form-control\">\n" +
    "                        <option ng-repeat=\"template in variants\" value=\"{{template.id}}\">\n" +
    "                            {{template.title}}\n" +
    "                        </option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "                --><div class=\"form-group answer-editor\" ng-repeat=\"variant in worksheetData.blocks[idActiveBlock].sections[idActiveSection].questions[idActiveQuestion].variants track by $index\"><ng-form class=\"input-answer\" name=\"userAnswer\"><div class=\"text-answer\" ng-class=\"{'has-error': userAnswer.answer.$invalid}\"><input type=\"text\" ng-change=\"changeAnswer()\" name=\"answer\" ng-model=\"variant.title\" placeholder=\"Имя\" class=\"form-control\" value=\"{{variant.title}}\" required><p ng-show=\"userAnswer.answer.$error.required\" class=\"help-block\">Имя слишком короткое</p></div><div class=\"score-answer\" ng-class=\"{'has-error': userAnswer.score.$invalid}\"><input type=\"number\" ng-change=\"changeAnswer()\" ng-model=\"variant.score\" name=\"score\" class=\"form-control\" value=\"{{variant.score}}\" ng-if=\"!isCertification\" required> <input type=\"number\" ng-change=\"changeAnswer()\" ng-model=\"variant.score\" name=\"score\" class=\"form-control\" value=\"{{variant.score}}\" min=\"0\" max=\"1\" ng-if=\"isCertification\" required><p ng-show=\"userAnswer.score.$error.required\" class=\"help-block number\">Должно быть число</p><p ng-show=\"userAnswer.score.$error.max\" class=\"help-block number\">Максимальное число 1</p><p ng-show=\"userAnswer.score.$error.min\" class=\"help-block number\">Минимальное число 0</p></div><div class=\"delete-answer\"><button class=\"btn btn-danger\" type=\"button\" ng-click=\"addDeleteAnswer($index)\">x</button></div></ng-form></div><div class=\"form-group\"><button type=\"button\" ng-click=\"addAnswer($index)\" class=\"btn btn-default add-option\" ng-hide=\"editTypeQuestion === ('textarea') || editTypeQuestion === ('input') || editTypeQuestion === ('files')\">Добавить вариант</button></div></div><div class=\"row\"><div class=\"col-xs-6 col-md-6\"><input class=\"btn btn-warning\" ng-click=\"clickButtonCancel('editQuestion')\" type=\"button\" value=\"Отменить\" ng-disabled=\"disabledCancel\"></div><div class=\"col-xs-6 col-md-6\"><input class=\"btn btn-danger\" type=\"button\" value=\"Удалить\" ng-click=\"deleteQuestion()\"></div></div></form></div><alert type=\"danger\" ng-hide=\"valid()\">Открытие панели для другого элемента недоступно.</alert></div><div class=\"mini-panel\" ng-hide=\"isEditBlock || isEditSection || isEditQuestion\"><span class=\"button-open-editor\" title=\"Открыть\" ng-click=\"showEditorViaButton()\"></span></div></div>"
  );

}]);
