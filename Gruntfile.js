module.exports = function(grunt) {

  grunt.initConfig({

    jshint: {
       all: ['Gruntfile.js', 'src/worksheet-editor.js']
    },
    
    ngtemplates:    {
      'worksheet-editor':  {
        src:        'src/templates/worksheet-editor.html',
        dest:       'worksheet-tmpl.js',
        options:    {
          htmlmin:  { collapseWhitespace: true, collapseBooleanAttributes: true }
        }
      }
    },

    concat: {
      'worksheet-editor':    {
        src:  [ 'src/worksheet-editor.js', 'worksheet-tmpl.js'],
        dest: 'dist/worksheet-editor.js'
      }
    },           
    
    uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: {
          'dist/worksheet-editor.min.js': ['dist/worksheet-editor.js']
        }
      }
    },
    
    copy: {
      main: {
        expand: true,
        cwd: 'src/',
        src: 'worksheet-editor.less',
        dest: 'dist/less/',
        flatten: true,
        filter: 'isFile'                
      }
    },
    
    less: {     
      production: {
        options: { 
          modifyVars: {
            'grid-img':'"../img/worksheet-editor.png"'
          }
        },        
        files: {
          "dist/worksheet-editor.css": "dist/less/worksheet-editor.less"
        }
      }
    },

    cssmin: {
      minify: {
        expand: true,
        cwd: 'dist/',
        src: ['*.css'],
        dest: 'dist/css/',
        ext: '.min.css'
      }
    },

    clean: ["worksheet-tmpl.js","dist/worksheet-editor.css"]

  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-less');


  grunt.registerTask('default', ['jshint','ngtemplates','concat','uglify', 'copy', 'less', 'cssmin', 'clean']);
};