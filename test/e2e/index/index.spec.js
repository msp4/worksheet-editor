describe('E2E: worksheet-editor', function() {
    var ptor;

    beforeEach(function() {
        browser.get('http://localhost:8000');
        ptor = protractor.getInstance();
    });    

    it('load page', function() {
        var name = by.id('worksheet-name');
        expect(ptor.isElementPresent(name)).toBe(true);
    });

    it('should have 4 blocks', function() {
        var elems = element.all(by.repeater('block in worksheetData.blocks'));
        expect(elems.count()).toBe(4);
    });

    it('not display panel', function() {
        expect(element(by.id('panel')).isDisplayed()).toBe(false);
    });

    describe('should be', function(){
        var block;
        
        beforeEach(function() {
            block = element(by.css('.tab-block ul li:nth-child(2)'));
            block.click();
        });
        
        it('active block', function() {
            expect(block.getAttribute('class')).toMatch(/active/);
        });         
        
        it('show worksheet-editor panel', function() {
            expect(element(by.id('panel')).isDisplayed()).toBe(true);
        });

        it('show red block when error, binding and save data when click other element', function() {
            element(by.model('nameEditBlock')).clear();

            expect(element(by.css('.tab-block ul li:nth-child(2) a')).getAttribute('class')).toMatch(/active-block/);

            element(by.model('nameEditBlock')).sendKeys('Новое имя блока');
            var elems = element.all(by.repeater('block in worksheetData.blocks').row(1).column('{{block.title}}'));

            elems.first().then(function(elm) {
                expect(elm.getText()).toEqual('2. Новое имя блока');
            });

            element(by.css('.tab-block ul li:nth-child(1)')).click();
            
            elems.first().then(function(elm) {
                expect(elm.getText()).toEqual('2. Новое имя блока');
            });
            
        });

        it('disabled cancel text / change text / cancel text', function() {
            expect(element(by.css('.btn.btn-warning')).getAttribute('disabled')).toBe('true');

            element(by.model('nameEditBlock')).sendKeys(' 3');

            var elems = element.all(by.repeater('block in worksheetData.blocks').row(1).column('{{block.title}}'));

            expect(element(by.css('.btn.btn-warning')).getAttribute('disabled')).toBe(null);

            elems.first().then(function(elm) {
                expect(elm.getText()).toEqual('2. Выявление потребности, консультация 3');
            });

            element(by.css('.btn.btn-warning')).click();

            elems.first().then(function(elm) {
                expect(elm.getText()).toEqual('2. Выявление потребности, консультация');
            });            

        });

        it('add block', function() {
            element(by.css('.tab-block ul:nth-child(2)')).click();
            var elems = element.all(by.repeater('block in worksheetData.blocks'));
            expect(elems.count()).toBe(5);            

        });

        it('delete block', function() {
            element(by.css('.btn.btn-danger')).click();
            var elems = element.all(by.repeater('block in worksheetData.blocks'));
            expect(elems.count()).toBe(3);
        });
    });

});