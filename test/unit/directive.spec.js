describe('Directive: worksheet-editor', function () {
  var element, $scope, el;

  // load module
  beforeEach(module('worksheet-editor'));

  beforeEach(inject( function ($compile, $rootScope) {

    // init new scope
    $scope = $rootScope.$new();
    $scope.data = {};

    // init directive
    element = angular.element('<worksheet-editor ng-model="data"></worksheet-editor>');
    el = $compile(element)($scope);
    $scope.$digest();
       
  }));

  describe('init', function() {
    
    describe('has', function() {
      
      it('addBlock()', function () {
        expect(el.html()).toContain("addBlock()");
      });

      it('access to the ngModel', function() {
        expect(el.isolateScope().ngModel).toBeDefined();
      });

      it('active element', function() {
        expect(el.isolateScope().idActiveBlock).toBe(0);
      });

    });

    describe('can', function(){
      var worksheet = {
                    'blocks': [{
                        'id': 'b0',
                        'title': 'Новый блок',
                        'comments': false,
                        'sections': [{
                            'id': 'b0s0',
                            'title': 'Новый раздел',
                            'active': true,
                            'comments': false,
                            'questions': [{
                                'variants': [{
                                                'id': 'b0s0v0',
                                                'title': 'Да',
                                                'score': 1
                                            },
                                            {
                                                'id': 'b0s0v1',
                                                'title': 'Нет',
                                                'score': 0
                                            },
                                            {
                                                'id': 'b0s0v3',
                                                'title': 'Не знаю',
                                                'score': 0
                                            }],
                                'id': 'b0s0q0',
                                'title': 'Вопрос',
                                'type': 'radio'                            
                              }]
                          }]
                      }]
                  };

      it('create block/section/question', function() {
        expect(el.isolateScope().defaultWorksheetData).toEqual(worksheet);
      });

      it('show panel', function() {
      	el.isolateScope().showEditorQuestion(0);

      	expect(el.isolateScope().isEditQuestion).toBeTruthy();
      });

      it('add block', function() {
      	el.isolateScope().addBlock();
      	expect(el.isolateScope().defaultWorksheetData.blocks.length).toBe(2);
      	expect(el.isolateScope().worksheetData.blocks.length).toBe(2);
      });

      it('add section', function() {
      	el.isolateScope().addSection();
      	expect(el.isolateScope().defaultWorksheetData.blocks[el.isolateScope().idActiveBlock].sections.length).toBe(2);
      	expect(el.isolateScope().worksheetData.blocks[el.isolateScope().idActiveBlock].sections.length).toBe(2);
      });

      it('add question', function() {
      	el.isolateScope().addQuestion();
      	expect(el.isolateScope().defaultWorksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions.length).toBe(2);
      	expect(el.isolateScope().worksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions.length).toBe(2);
      });

      it('add answer | not save in original object', function() {
      	el.isolateScope().idActiveQuestion = 0;
      	el.isolateScope().addAnswer();
      	expect(el.isolateScope().worksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions[el.isolateScope().idActiveQuestion].variants.length).toBe(4);
      	expect(el.isolateScope().defaultWorksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions[el.isolateScope().idActiveQuestion].variants.length).toBe(3);
      });
      
      it('add answer | with save in original object', function() {
      	el.isolateScope().idActiveQuestion = 0;
      	el.isolateScope().addAnswer();
      	el.isolateScope().showEditorSection(0);
      	expect(el.isolateScope().worksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions[0].variants.length).toBe(4);
      	expect(el.isolateScope().defaultWorksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions[0].variants.length).toBe(4);
      });      

      it('delete block', function() {
      	el.isolateScope().deleteBlock();
      	expect(el.isolateScope().defaultWorksheetData.blocks.length).toBe(0);
      	expect(el.isolateScope().worksheetData.blocks.length).toBe(0);     	
      });

      it('delete section', function() {
      	el.isolateScope().deleteSection();
      	expect(el.isolateScope().defaultWorksheetData.blocks[el.isolateScope().idActiveBlock].sections.length).toBe(0);
      	expect(el.isolateScope().worksheetData.blocks[el.isolateScope().idActiveBlock].sections.length).toBe(0);      	
      });

      it('delete question', function() {
      	el.isolateScope().idActiveQuestion = 0;
      	el.isolateScope().deleteQuestion();
      	expect(el.isolateScope().defaultWorksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions.length).toBe(0);
      	expect(el.isolateScope().worksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions.length).toBe(0);        	
      });

      it('delete answer', function() {
      	el.isolateScope().idActiveQuestion = 0;
      	el.isolateScope().addDeleteAnswer(2);
      	expect(el.isolateScope().worksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions[el.isolateScope().idActiveQuestion].variants.length).toBe(2);
      	expect(el.isolateScope().defaultWorksheetData.blocks[el.isolateScope().idActiveBlock].sections[el.isolateScope().idActiveSection].questions[el.isolateScope().idActiveQuestion].variants.length).toBe(3);      	
      });
    });

	describe('change', function(){
		beforeEach(function() {
			el.isolateScope().addBlock();
            el.isolateScope().$apply();
		});

		it('disabled button cancel', function() {
			expect(el.isolateScope().disabledCancel).toBeTruthy();
		});

		it('save in clone and original object |  cancel button active', function() {
            el.isolateScope().dataForButtonCancel = { title: 'Новый блок' };
            el.isolateScope().nameEditBlock = 'New name create';
			el.isolateScope().changeBlock();


			expect(el.isolateScope().disabledCancel).toBeFalsy();
			expect(el.isolateScope().worksheetData.blocks[1].title).toBe('New name create');
			expect(el.isolateScope().defaultWorksheetData.blocks[1].title).toBe('New name create');
            expect(el.isolateScope().dataForButtonCancel.title).toBe('Новый блок');

		});

		it('save in original object', function() {
            el.isolateScope().nameEditBlock = 'New name create';
			el.isolateScope().changeBlock();

            expect(el.isolateScope().disabledCancel).toBeFalsy();

			el.isolateScope().showEditorSection(0);


			expect(el.isolateScope().worksheetData.blocks[1].title).toBe('New name create');
			expect(el.isolateScope().defaultWorksheetData.blocks[1].title).toBe('New name create');
		});

		it('cancel', function() {
            el.isolateScope().dataForButtonCancel = { title: 'Новый блок' };
			el.isolateScope().nameEditBlock = 'New name create';
			el.isolateScope().changeBlock();
			el.isolateScope().clickButtonCancel('editBlock');
			expect(el.isolateScope().worksheetData.blocks[1].title).toBe('Новый блок');
			expect(el.isolateScope().defaultWorksheetData.blocks[1].title).toBe('Новый блок');
		});

		it('valid', function() {
			el.isolateScope().nameEditBlock = 'New name create';
			el.isolateScope().changeBlock();
            el.isolateScope().$apply();
			expect(el.isolateScope().valid()).toBeTruthy();

		});

		it('invalid', function() {
			el.isolateScope().nameEditBlock = '';
			el.isolateScope().changeBlock();
			el.isolateScope().$apply();
			expect(el.isolateScope().valid()).toBeFalsy();
		});
	  
	});

  });

});