## worksheet-editor - angularjs directive [![Build Status](https://travis-ci.org/msp4/worksheet-editor.svg?branch=master)](https://travis-ci.org/msp4/worksheet-editor)

[Plunker demo](http://plnkr.co/edit/1bAVRhSCQDFL6MTX9Hdk)

### Install via Bower
```
bower install git://github.com/msp4/worksheet-editor.git
```

### Update html template
```html
  <link rel="stylesheet" href="bower_components/bootstrap-css-only/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome-css/css/font-awesome.min.css">
  <link rel="stylesheet" href="bower_components/worksheet-editor/dist/css/worksheet-editor.min.css">
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
  <script src="bower_components/angular/angular.min.js"></script>
  <script src="bower_components/angular-resource/angular-resource.min.js"></script>
  <script src="bower_components/angular-ui-sortable/sortable.min.js"></script>
  <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
  <script src="bower_components/worksheet-editor/dist/worksheet-editor.min.js"></script>
```

### Instruction

For instruction must be connected https://github.com/fraywing/textAngular/ to the main project. Tested with version 1.2.2.

### Register directive in the controller

```javascript
var myModule = angular.module('myModule', ['worksheet-editor']);

myModule.controller('myModuleCtrl', function ($scope, $resource) {
    var Worksheet = $resource('path/*.json');
    $scope.data = Worksheet.get();
});
```

### Use directive in the view

```html
<worksheet-editor ng-model="data"></worksheet-editor>
```

####for certification

```html
<worksheet-editor type="'certification'" ng-model="data"></worksheet-editor>
```
